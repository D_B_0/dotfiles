Config { font = "xft:RobotoMono Nerd Font:size=13:antialias=true"
       , additionalFonts = [ "xft:Font Awesome 5 Free Solid:size=16:antialias=true:hinting=true"
                           , "xft:Font Awesome 5 Free:size=16:antialias=true:hinting=true"
                           , "xft:Font Awesome 5 Brands:size=16:antialias=true:hinting=true"
                           ]
       , borderColor = "black"
       , border = TopB
       , fgColor = "#f8f8f2"
       -- No transparency
       , bgColor = "#282a36"
       , alpha = 255
       --, bgColor = "#1a3619" -- RGB: when alpha is not 255, G and B values have to be inverted
       , position = Top
       , textOffset = -1
       , iconOffset = -1
       , lowerOnStart = True
       , pickBroadest = False
       , persistent = False
       , hideOnStart = False
       , iconRoot = ".config/xmobar/"
       , allDesktops = True
       , overrideRedirect = True
       , commands = [ Run Network "wlp4s0" [ "-S", "true"
                                           , "-t", "<fn=1>\xf1eb</fn>\ 
                                                    \ <rx> <fn=1>\xf309</fn>\
                                                    \<fn=1>\xf30c</fn> <tx> "
                                           ] 10
                    , Run Network "eno1" [ "-S", "true"
                                         , "-t", "<fn=1>\xf796</fn>\ 
                                                  \ <rx> <fn=1>\xf309</fn>\
                                                  \<fn=1>\xf30c</fn> <tx> "
                                         ] 10
                    , Run Cpu [ "-t", "<fn=1>\xf2db</fn> <total>% <fn=1>\xf2c9</fn>"
                              ] 10
                    , Run Memory [ "-t", "<fn=1>\xf538</fn> <usedratio>%"
                                 ] 10
                    , Run Swap [ "-t", "<fn=1>\xf079</fn> <usedratio>%"
                               ] 10
                    , Run Date "<action=`gnome-calendar`>\
                                 \%d/%m/%Y\
                               \</action> \
                               \<action=`gnome-clocks`>\
                                 \%H:%M:%S\
                               \</action>" "date" 10
                    , Run Com "scripts/upgradable_packages.sh" [] "up_pkg" 3000 -- 5 min
                    , Run Com "scripts/gpu_temp.sh" [] "gpu_temp" 10
                    , Run Com "scripts/gpu_usage.sh" [] "gpu_usage" 10
                    , Run Com "scripts/cpu_temp.sh" [] "cpu_temp" 10
                    , Run UnsafeStdinReader
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "\
                    \ %UnsafeStdinReader%\
                    \}{\
                    \<action=`alacritty -e sudo nethogs`>\
                      \<fc=#ff5555><box width=2 type=Bottom>\
                        \ %wlp4s0%\
                        \%eno1%\
                      \</box> </fc>\
                    \</action>\
                    \<action=`alacritty -e htop`>\
                      \<fc=#50fa7b><box width=2 type=Bottom> %cpu% %cpu_temp% </box> </fc>\
                      \<fc=#ffb86c><box width=2 type=Bottom> %memory% %swap% </box> </fc>\
                    \</action>\
                    \<fc=#8be9fd><box width=2 type=Bottom> <icon=gpu.xbm/> %gpu_usage%\x25 %gpu_temp%°C </box> </fc>\
                    \<action=`alacritty -e sudo pacman -Syyu \-\-noconfirm`>\
                      \<fc=#bd93f9><box width=2 type=Bottom> <icon=bell.xbm/> %up_pkg% </box> </fc>\
                    \</action>\
                    \<fc=#f1fa8c><box width=2 type=Bottom> %date% </box> </fc>\
                    \"
       }
