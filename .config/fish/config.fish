function startup_graphics
        figlet -f /usr/share/figlet/fonts/slant "$USER" | lolcat
end

if status is-interactive
        set fish_greeting

        set -gx GUI_EDITOR emacs
        set -gx EDITOR nvim

        set -gx PATH "$HOME/.local/bin:$HOME/.cargo/bin:$PATH"

        oh-my-posh --init --shell fish --config ~/.poshthemes/dracula.omp.json | source
        oh-my-posh disable notice
        alias ls='ls -lAh --color=auto'
        alias ll='lsd -lAh'

        alias gs='git status'
        alias gcm='git commit -m'
        alias gaa='git add .'

        alias git-dotfiles='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

        alias cls='clear && startup_graphics'

        startup_graphics
end

thefuck --alias | source
