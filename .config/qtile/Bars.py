from libqtile import bar, widget
from libqtile.lazy import lazy

from Terminal import terminal
import CpuTempGraphWidget
import GpuGraphWidget
import GpuTempGraphWidget
import MyGraphs
from Colors import colors


def icon(char):
    return "<span font='Font Awesome 6 Free'>{}</span>".format(char)


def open_nethogs():
    lazy.spawn(terminal + " -e nethogs")


def open_htop():
    lazy.spawn(terminal + " -e htop")


network_callbacks = {
    "Button1": open_nethogs
},

htop_callbacks = {
    "Button1": open_htop
},

bottom_bar = bar.Bar(
    [
        widget.CurrentLayout(
            foreground=colors["cyan"],
        ),
        widget.GroupBox(
            hide_unused=False,
            use_mouse_wheel=False,
            disable_drag=True,
            active=colors["comment"],
            this_current_screen_border=colors["green"],
            this_screen_border=colors["yellow"],
            highlight_method="text",
        ),
        # widget.WindowName(),
        widget.Spacer(),
        # Network
		MyGraphs.NetGraph(
			bandwidth_type="down",
			border_width=0,
			line_width=1,
			fill_color=colors["red_transparent"],
			graph_color=colors["red"],
			mouse_callbacks=network_callbacks,
		),
        widget.TextBox(
            foreground=colors["red"],
            fmt=icon("\uf309") + icon("\uf30c")
        ),
        MyGraphs.NetGraph(
            bandwidth_type="up",
            border_width=0,
            line_width=1,
            fill_color=colors["red_transparent"],
            graph_color=colors["red"],
            mouse_callbacks=network_callbacks,
        ),
        # CPU
        widget.TextBox(
            foreground=colors["green"],
            fmt=icon("\uf2db"),
        ),
        MyGraphs.CPUGraph(
            border_width=0,
            line_width=1,
            fill_color=colors["green_transparent"],
            graph_color=colors["green"],
        ),
        widget.TextBox(
            foreground=colors["green"],
            fmt=icon("\uf2c9"),
        ),
        CpuTempGraphWidget.CPUTempGraph(
            border_width=0,
            line_width=1,
            fill_color=colors["green_transparent"],
            graph_color=colors["green"],
        ),
        # Ram
        widget.TextBox(
            foreground=colors["orange"],
            fmt=icon("\uf538"),
        ),
		MyGraphs.MemoryGraph(
            border_width=0,
            line_width=1,
            fill_color=colors["orange_transparent"],
            graph_color=colors["orange"],
		),
        widget.TextBox(
            foreground=colors["orange"],
            fmt=icon("\uf079"),
        ),
		MyGraphs.SwapGraph(
            border_width=0,
            line_width=1,
            fill_color=colors["orange_transparent"],
            graph_color=colors["orange"],
		),
        # Gpu
        widget.TextBox(
            foreground=colors["cyan"],
            fmt=icon("\uf863"),
        ),
		GpuGraphWidget.GPUGraph(
            border_width=0,
            line_width=1,
            fill_color=colors["cyan_transparent"],
            graph_color=colors["cyan"],
		),
        widget.TextBox(
            foreground=colors["cyan"],
            fmt=icon("\uf2c9"),
        ),
		GpuTempGraphWidget.GPUTempGraph(
            border_width=0,
            line_width=1,
            fill_color=colors["cyan_transparent"],
            graph_color=colors["cyan"],
		),
        # Updates
        widget.CheckUpdates(
            colour_have_updates=colors["purple"],
            colour_no_updates=colors["purple"],
            display_format=" {updates}",
            no_update_string=" 0",
            # distro="Arch_yay",
            update_interval=500,
            # yay updates
            custom_command="sudo pacman -Sy >> /dev/null; yay -Qu | awk '/^[^:]/'",
            execute=terminal + " -e yay -Su",
            # pacman updates
            # custom_command="sudo pacman -Sy >> /dev/null && pacman -Qu",
            # execute=terminal + " -e pacman -Su",
        ),
        widget.Systray(),
        # Time
        widget.Clock(
            foreground=colors["yellow"],
            format=" %H:%M:%S  %d/%m/%Y",
            mouse_callbacks={
                "Button1": lambda: lazy.spawn("gnome-calendar")
            },
        ),
    ],
    24,
)


def preprocess_text(text):
    for string in [" - OSS", " — Mozilla Firefox"]:
        text = text.replace(string, "")
    return text


top_bar = bar.Bar(
    [
        widget.TaskList(
            margin_y=2,
            highlight_method="block",
            title_width_method="uniform",
            border=colors["current_line"],
            txt_floating="🗗 ",
            txt_maximized="🗖 ",
            txt_minimized="— ",
            parse_text=preprocess_text,
        ),
    ],
    28,
)
