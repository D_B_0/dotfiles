import os
import subprocess
from libqtile.widget import base


class Gpu(base.InLoopPollText):
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        (
            'format', '{usage}% {temp}°C',
            'Display string format. Two options available:  '
            '``{temp}`` and ``{usage}``'
        ),
        ('update_interval', 1, 'Update interval in seconds'),
    ]

    def __init__(self, **config):
        base.InLoopPollText.__init__(self, **config)
        self.add_defaults(Gpu.defaults)

    def poll(self):
        home = os.path.expanduser('~')
        data = {
            "usage": subprocess.getoutput(home + "/scripts/gpu_usage.sh"),
            "temp": subprocess.getoutput(home + "/scripts/gpu_temp.sh"),
        }
        return self.format.format(**data)
