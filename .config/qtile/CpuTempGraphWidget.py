from libqtile.widget.graph import _Graph
from libqtile.widget import base
import psutil

class CPUTempGraph(_Graph):
    """Display CPU temperature graph.
    """

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
    ]

    fixed_upper_bound = True

    def __init__(self, **config):
        _Graph.__init__(self, 50, **config)
        self.add_defaults(CPUTempGraph.defaults)
        self.maxvalue = 100

    def _getvalue(self):
        temperature_list = {}
        temps = psutil.sensors_temperatures()
        for kernel_module in temps:
            for sensor in temps[kernel_module]:
                return sensor.current
        return None

    def update_graph(self):
        push_value = self._getvalue()
        if push_value:
            self.push(push_value)
        else:
            self.push(self.values[0])
