import os
import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile import hook

import Bars
from Terminal import terminal
from Colors import colors

mod = "mod4"


@hook.subscribe.startup
def start():
    home = os.path.expanduser('~')
    subprocess.call([home + '/scripts/random_wallpaper.sh'])


keys = [
    # Run a program
    Key([mod, "shift"], "space", lazy.spawn("rofi -show run"),
        desc="Spawn a window using rofi"),
    Key([mod], "space", lazy.spawn("rofi -show drun"),
        desc="Spawn a command using rofi"),

    # Screenshot
    Key([], "Print", lazy.spawn("gnome-screenshot"), desc="Take a screenshot"),
    Key([mod], "Print", lazy.spawn("gnome-screenshot -i"),
        desc="Take a screenshot interactively"),

    # Open programs
    Key([mod], "f", lazy.spawn("firefox"), desc="Open Firefox"),
    Key([mod], "s", lazy.spawn("steam"), desc="Open Steam"),
    Key([mod], "d", lazy.spawn("discord"), desc="Open Discord"),

    # Update system
    Key([mod], "u", lazy.spawn(terminal + " -e yay -Syu"), desc="Update system"),

    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "Tab", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    # Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
    #   desc="Toggle between split and unsplit sides of stack"),
    Key([mod, "shift"], "Return",
        lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Left", lazy.prev_layout(), desc="Previous layout"),
    Key([mod], "Right", lazy.next_layout(), desc="Next layout"),
    Key([mod], "F11", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),

    Key([mod], "q", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
]

group_names = "123456789"
groups = [Group(i) for i in group_names]


# def switch_to_group(name):
#     commands = lazy.spawn("zenity --info --text='Switching to group'")
#     if lazy.group.info()["name"] != name:
#         commands.append(lazy.group[name].toscreen())
#     return commands


for i in group_names:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i, lazy.group[i].toscreen(),
            desc="Switch to group {}".format(i)),

        # # mod1 + shift + letter of group = move focused window to group
        Key([mod, "shift"], i, lazy.window.togroup(i),
            desc="Move focused window to group {}".format(i)),
    ])

layout_defaults = {
    "border_normal": "44475a",
    "border_focus": "50fa7b",
    "border_width": 4,
}

layouts = [
    layout.Max(),
    layout.Columns(
        margin=4,
        margin_on_single=0,
        **layout_defaults,
    ),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    layout.MonadWide(
        ratio=0.70,
        **layout_defaults,
    ),
    # layout.RatioTile(),
    # layout.Tile(
    #     **layout_defaults,
    # ),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='RobotoMono Nerd Font',
    fontsize=16,
    foreground=colors["foreground"],
    background=colors["background"],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=Bars.top_bar,
        bottom=Bars.bottom_bar
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.toggle_floating())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='kyte'),  # kyte
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(title='branchdialog'),  # gitk
    Match(wm_class='galculator'),  # Galculator
    Match(title='Calendar'),  # gnome
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
