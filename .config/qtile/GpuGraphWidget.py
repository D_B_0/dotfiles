import os
import subprocess
from libqtile.widget.graph import _Graph
from libqtile.widget import base

class GPUGraph(_Graph):
    """Display GPU usage graph.
    """

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
    ]

    fixed_upper_bound = True

    def __init__(self, **config):
        _Graph.__init__(self, width=50, **config)
        self.add_defaults(GPUGraph.defaults)
        self.maxvalue = 100

    def _getvalue(self):
        try:
            return float(subprocess.getoutput(os.path.expanduser('~/scripts/gpu_usage.sh')))
        except ValueError:
            return None

    def update_graph(self):
        push_value = self._getvalue()
        if push_value:
            self.push(push_value)
        else:
            self.push(0)
