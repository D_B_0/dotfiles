# I use these graphs, wich are almost identical to the base qtile ones because i want to resize them

import itertools
import operator
import time
from os import statvfs

import cairocffi
import psutil

from libqtile.log_utils import logger
from libqtile.widget import base
from libqtile.widget.graph import _Graph


class CPUGraph(_Graph):
    """Display CPU usage graph.

    Widget requirements: psutil_.

    .. _psutil: https://pypi.org/project/psutil/
    """

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ("core", "all", "Which core to show (all/0/1/2/...)"),
    ]

    fixed_upper_bound = True

    def __init__(self, **config):
        _Graph.__init__(self, 50, **config)
        self.add_defaults(CPUGraph.defaults)
        self.maxvalue = 100
        self.oldvalues = self._getvalues()

    def _getvalues(self):
        if isinstance(self.core, int):
            if self.core > psutil.cpu_count() - 1:
                raise ValueError("No such core: {}".format(self.core))
            cpu = psutil.cpu_times(percpu=True)[self.core]
        else:
            cpu = psutil.cpu_times()

        user = cpu.user * 100
        nice = cpu.nice * 100
        sys = cpu.system * 100
        idle = cpu.idle * 100

        return (int(user), int(nice), int(sys), int(idle))

    def update_graph(self):
        nval = self._getvalues()
        oval = self.oldvalues
        busy = nval[0] + nval[1] + nval[2] - oval[0] - oval[1] - oval[2]
        total = busy + nval[3] - oval[3]
        # sometimes this value is zero for unknown reason (time shift?)
        # we just sent the previous value, because it gives us no info about
        # cpu load, if it's zero.

        if total:
            push_value = busy * 100.0 / total
            self.push(push_value)
        else:
            self.push(self.values[0])
        self.oldvalues = nval


class MemoryGraph(_Graph):
    """Displays a memory usage graph.

    Widget requirements: psutil_.

    .. _psutil: https://pypi.org/project/psutil/
    """

    orientations = base.ORIENTATION_HORIZONTAL
    fixed_upper_bound = True

    def __init__(self, **config):
        _Graph.__init__(self, 50, **config)
        val = self._getvalues()
        self.maxvalue = val["MemTotal"]

        mem = val["MemTotal"] - val["MemFree"] - val["Buffers"] - val["Cached"]
        self.fulfill(mem)

    def _getvalues(self):
        val = {}
        mem = psutil.virtual_memory()
        val["MemTotal"] = int(mem.total / 1024 / 1024)
        val["MemFree"] = int(mem.free / 1024 / 1024)
        val["Buffers"] = int(mem.buffers / 1024 / 1024)
        val["Cached"] = int(mem.cached / 1024 / 1024)
        return val

    def update_graph(self):
        val = self._getvalues()
        self.push(val["MemTotal"] - val["MemFree"] - val["Buffers"] - val["Cached"])


class SwapGraph(_Graph):
    """Display a swap info graph.

    Widget requirements: psutil_.

    .. _psutil: https://pypi.org/project/psutil/
    """

    orientations = base.ORIENTATION_HORIZONTAL
    fixed_upper_bound = True

    def __init__(self, **config):
        _Graph.__init__(self, 50, **config)
        val = self._getvalues()
        self.maxvalue = val["SwapTotal"]
        swap = val["SwapTotal"] - val["SwapFree"]
        self.fulfill(swap)

    def _getvalues(self):
        val = {}
        swap = psutil.swap_memory()
        val["SwapTotal"] = int(swap.total / 1024 / 1024)
        val["SwapFree"] = int(swap.free / 1024 / 1024)
        return val

    def update_graph(self):
        val = self._getvalues()

        swap = val["SwapTotal"] - val["SwapFree"]

        # can change, swapon/off
        if self.maxvalue != val["SwapTotal"]:
            self.maxvalue = val["SwapTotal"]
            self.fulfill(swap)
        self.push(swap)


class NetGraph(_Graph):
    """Display a network usage graph.

    Widget requirements: psutil_.

    .. _psutil: https://pypi.org/project/psutil/"""

    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ("interface", "auto", "Interface to display info for ('auto' for detection)"),
        ("bandwidth_type", "down", "down(load)/up(load)"),
    ]

    def __init__(self, **config):
        _Graph.__init__(self, 50, **config)
        self.add_defaults(NetGraph.defaults)
        if self.interface == "auto":
            try:
                self.interface = self.get_main_iface()
            except RuntimeError:
                logger.warning(
                    "NetGraph - Automatic interface detection failed, falling back to 'eth0'"
                )
                self.interface = "eth0"
        if self.bandwidth_type != "down" and self.bandwidth_type != "up":
            raise ValueError("bandwidth type {} not known!".format(self.bandwidth_type))
        self.bytes = 0
        self.bytes = self._get_values()

    def _get_values(self):
        net = psutil.net_io_counters(pernic=True)
        if self.bandwidth_type == "up":
            return net[self.interface].bytes_sent
        if self.bandwidth_type == "down":
            return net[self.interface].bytes_recv

    def update_graph(self):
        val = self._get_values()
        change = val - self.bytes
        self.bytes = val
        self.push(change)

    @staticmethod
    def get_main_iface():
        # XXX: psutil doesn't have the facility to get the main interface,
        # so I'll just return the interface that has received the most traffic.
        #
        # I could do this with netifaces, but that's another dependency.
        #
        # Oh. and there is probably a better way to do this.

        net = psutil.net_io_counters(pernic=True)
        iface = {}
        for entry in net:
            iface[entry] = net[entry].bytes_recv
        return sorted(iface.items(), key=operator.itemgetter(1))[-1][0]
