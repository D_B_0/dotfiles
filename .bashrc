#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Powerline

# powerline-daemon -q
# POWERLINE_BASH_CONTINUATION=1
# POWERLINE_BASH_SELECT=1
# . /usr/share/powerline/bindings/bash/powerline.sh

# Oh My Posh

eval "$(oh-my-posh --init --shell bash --config ~/.poshthemes/dracula.omp.json)"

# Enviroment vars

export GUI_EDITOR=emacs
export EDITOR=nvim

export PATH="$HOME/.local/bin:$PATH"

# Aliases

alias ls='ls -lA --color=auto'
alias ll='lsd -lA'

alias gs='git status'
alias gcm='git commit -m'
alias gaa='git add .'

alias git-dotfiles='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

alias krita='flatpak run org.kde.krita'

alias cls='clear && startup_graphics'

startup_graphics() {
    figlet -f /usr/share/figlet/fonts/slant "$USER" | lolcat
}

startup_graphics

PS1='[\u@\h \W]\$ '
