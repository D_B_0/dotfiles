#! /bin/bash

RAW_TEMP=$(cat /sys/class/thermal/thermal_zone1/temp)
TEMP_LENGTH=${#RAW_TEMP}
INT_TEMP=${RAW_TEMP::-3}
FRAC_TEMP=${RAW_TEMP:`expr $TEMP_LENGTH - 3`:-2}
echo $INT_TEMP.$FRAC_TEMP\C
