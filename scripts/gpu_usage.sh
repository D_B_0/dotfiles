#! /bin/bash

TEMP=$(printf '%s' $(nvidia-smi --format=csv,noheader --query-gpu=utilization.gpu | awk '{print $1}'))
echo $TEMP 
